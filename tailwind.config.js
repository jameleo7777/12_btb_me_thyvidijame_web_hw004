/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,js,jsx}"],
  theme: {
    extend: {
      color:{
        'Navy': '#bg-gradient-to-t from-gray-800 via-blue-600 to-yellow-900'
      },
    },
  },
  plugins: [],
}
