import React, { Component } from "react";
import TableData from "./TableData";

export default class Display extends Component {
  constructor() {
    super();
    this.state = {
      registerName: [],

      userEmail: "",
      userName: "",
      userAge: "",
      status: "",
    };
  }

  OnStatus = (id) => {
    this.state.registerName.forEach((datalist) => {
      if (datalist.id === id) {
        datalist.status = datalist.status === "Pending" ? "Done" : "Pending";
      }
    });
    this.setState({});
  };
  // Get Input from email
  emailHandler = (email) => {
    this.setState({
      userEmail: email.target.value,
    });
  };
  // Get Input from name
  nameHandler = (name) => {
    this.setState({
      userName: name.target.value,
    });
  };
  // Get Input from age
  ageHandler = (age) => {
    this.setState({
      userAge: age.target.value,
    });
  };
  // Unpack array
  onRegister = () => {
    const newUser = {
      id: this.state.registerName.length + 1,
      userEmail: this.state.userEmail,
      userName: this.state.userName,
      userAge: this.state.userAge,
      status: "Pending",
    };
    this.setState({
      registerName: [...this.state.registerName, newUser],
    });
  };

  render() {
    return (
      <div>
        <div class="text-5xl text-center pt-10 font-bold  font-[Lexend]">
          <h1 class="bg-gradient-to-r from-blue-600 via-green-500 to-indigo-400 inline-block text-transparent bg-clip-text">Please fill your Information</h1>
        </div>

        {/* Input Field */}

        <div class=" w-[70%] m-auto mt-4 font-[Lexend]">
          <label
            for="input-group-1"
            class="block mb-2 font-medium text-gray-900 dark:text-black text-xl"
          >
            Your Email
          </label>
          <div class="relative mb-6">
            <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
              <svg
                aria-hidden="true"
                class="w-5 h-5 text-gray-600 dark:text-gray-600"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z"></path>
                <path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z"></path>
              </svg>
            </div>
            <input
              type="text"
              required
              id="input-group-1"
              class=" text-gray-900 text-sm rounded-md  block w-full pl-10 p-4  dark:bg-white dark:border-gray-600 dark:placeholder-black dark:text-black font-semibold"
              placeholder="name@gmail.com"
              onChange={this.emailHandler}
            />
          </div>

          <label
            for="input-group-1"
            class="block mb-2 font-medium text-gray-600 dark:text-black text-xl"
          >
            Username
          </label>
          <div class="relative mb-6">
            <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none text-gray-600 font-bold">
              @
            </div>
            <input
              type="text"
              required
              id="input-group-1"
              class=" text-gray-900 text-sm rounded-md  block w-full pl-10 p-4  dark:bg-white dark:border-gray-600 dark:placeholder-black dark:text-black font-semibold"
              placeholder="Enter your name"
              onChange={this.nameHandler}
            />
          </div>

          <label
            for="input-group-1"
            class="block mb-2 font-medium text-gray-900 dark:text-black text-xl"
          >
            Age
          </label>
          <div className="relative mb-6">
            <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
              💕
            </div>
            <input
              type="text"
              required
              id="input-group-1"
              className=" text-gray-900 text-sm rounded-md  block w-full pl-10 p-4  dark:bg-white dark:border-gray-600 dark:placeholder-black dark:text-black font-semibold"
              placeholder="Enter your age"
              onChange={this.ageHandler}
            />
          </div>

          {/* Register Button */}

          <div className="text-center">
            <button
              onClick={this.onRegister}
              type="button"
              class="bg-white  p-3 mt-6 w-[250px] rounded-md font-bold text-center bg-gradient-to-r hover:from-gray-400 hover:to-sky-700 border-2 border-blue-500 hover:text-white"
            >
              Register
            </button>
          </div>
        </div>

        <TableData data={this.state.registerName} OnStatus={this.OnStatus} />
      </div>
    );
  }
}
