import React, { Component } from 'react'
import Swal from 'sweetalert2'
import 'animate.css';

export default class TableData extends Component {
   
    
    OnAlert =(datalist)=>{
        if(datalist === "null" ? ` `:datalist){
            Swal.fire({
                title:  ` ID : `+datalist.id+  ` <br> Email : `+datalist.userEmail+  ` <br> Username : `+datalist.userName+  ` <br> Age : `+datalist.userAge,
                width: 600,
                padding: '3em',
                color: '#3E54AC',
                showClass: {  
                  popup: 'animate__animated animate__fadeInDown'
                },
                hideClass: {
                  popup: 'animate__animated animate__fadeOutLeft'
                }
              })
        }
    
      }

  render() {
    
    return (
      <div>
        
<div class="relative overflow-x-auto mt-12 text-center font-[Poppins]">
    <table class="w-[70%] text-xl text-left text-black dark:text-black m-auto p-4">
        <thead class="text-md text-black dark:bg-slate-500 dark:text-white bg-gradient-to-r from-blue-600 via-green-500 to-indigo-400">
            <tr>
                <th scope="col" class="p-5 text-center rounded-l-md ">
                    ID
                </th>
                <th scope="col" class="p-5 text-center">
                    EMAIL
                </th>
                <th scope="col" class="p-5 text-center">
                   USERNAME
                </th>
                <th scope="col" class="p-5 text-center">
                    AGE
                </th>
                <th scope="col" class="p-5 text-center rounded-r-md">
                    ACTION
                </th>
            </tr>
        </thead>
        <tbody>
            {/* Display Data */}
                {this.props.data.map((datalist)=>(
                    <tr  class="p-10 h-20 text-sm text-center even:bg-pink-100 odd:bg-white" key={datalist.id}  >
                        <td class="rounded-l-md">{datalist.id === " "? "null":datalist.id}</td>
                        <td>{datalist.userEmail === ""?`null`:datalist.userEmail}</td>
                        <td>{datalist.userName === ""?`null`:datalist.userName}</td>    
                        <td>{datalist.userAge === ""?`null`:datalist.userAge}</td>
                        <td class="text-center text-sm text-white font-bold rounded-r-md">
                        <button onClick={()=> this.props.OnStatus(datalist.id)} class={`${datalist.status === "Pending" ? ` text-white   font-medium w-40
                                        rounded-lg text-sm pl-12 pt-3 pb-3  inline-flex items-center mr-2
                                         dark:bg-red-500` : ` text-white font-medium  w-40 mx-auto
                                         rounded-lg text-sm pl-14 pt-3 pb-3  inline-flex items-center mr-2
                                          dark:bg-green-600 `}`}>{datalist.status}</button>
                            <button onClick={()=>this.OnAlert(datalist)} type='button' class="bg-blue-500 rounded-md p-3 ml-2 w-40 ">Show More</button>
                        </td>
                    </tr>
                   
                ))}
        </tbody>
    </table>
</div>

      </div>
    )
  }
}
