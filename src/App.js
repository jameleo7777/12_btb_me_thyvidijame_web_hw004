import "./App.css";
import Display from "./components/Display";

function App() {
  return (
    <div className="bg-[#C4DDFF] min-h-screen">
         <Display/>
    </div>
  
  );
}

export default App;
